﻿using CommandLine;
using Moravia.Homework.FormatConverter.Enum;

namespace Moravia.Homework
{
    public class ProgramOptions
    {
        [Option('i', "input", Required = true, HelpText = "Path or url to source file")]
        public string InputPath { get; set; }

        [Option('r', "reader", Required = false, Default = ReaderProvider.FileSystem,
            HelpText = "FileSystem, HttpAdress, AzureBlobStorage")]
        public ReaderProvider ReaderOption { get; set; }

        [Option('o', "output", Required = false, HelpText = "Output path")]
        public string OutputPath { get; set; }

        [Option('w', "writer", Required = false, Default = WriterProvider.FileSystem,
            HelpText = "FileSystem, AzureBlobStorage")]
        public WriterProvider WriterOption { get; set; }

        [Option('c', "conversion", Required = true,
            HelpText = "Xml2Json, Xml2JsonCS, Json2Xml")]
        public ConversionDirection Direction { get; set; }
    }
}
