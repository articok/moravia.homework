﻿using CommandLine;
using Microsoft.Extensions.Configuration;
using Moravia.Homework.FormatConverter;
using Moravia.Homework.Model;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Moravia.Homework
{
    class Program
    {
        static async Task Main(string[] args)
        {
            SetupLogger();

            try
            {
                await Parser.Default.ParseArguments<ProgramOptions>(args)
                    .WithParsedAsync(o => RunConversion(o));
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "An unhandled exception occurred.");
                Environment.Exit(-1);
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        static async Task RunConversion(ProgramOptions options)
        {
            var result = await Converter<Document>
                .NewConverter
                .SetInput(options.InputPath, options.ReaderOption)
                .SetDirection(options.Direction)
                .SetOutput(options.OutputPath, options.WriterOption)
                .Convert();

            Environment.ExitCode = result.ConversionSuccessful ? 0 : -1;
        }

        private static void SetupLogger()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }
    }
}
