﻿
using Moravia.Homework.FormatConverter.Enum;

namespace Moravia.Homework.FormatConverter.Exception
{
    public class ConversionException : System.Exception
    {
        public ConversionError Error { get; private set; }

        public ConversionException()
        {
        }

        public ConversionException(ConversionError error)
        {
            Error = error;
        }
    }
}
