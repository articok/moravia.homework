﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Moravia.Homework.FormatConverter.DataWriter
{
    public class AzureBlobWriter : IDataWriter
    {
        private readonly CloudBlobClient _blobClient;
        private readonly CloudBlobContainer _container;

        public AzureBlobWriter(string cloudCredentials, string containerName)
        {
            _blobClient = CloudStorageAccount.Parse(cloudCredentials).CreateCloudBlobClient();
            _container = _blobClient.GetContainerReference(containerName);
        }

        public async Task WriteAsync(string location, string payload)
        {
            var blob = _container.GetBlockBlobReference(location);
            await blob.UploadTextAsync(payload);
        }
    }
}
