﻿using System.Threading.Tasks;

namespace Moravia.Homework.FormatConverter.DataWriter
{
    public interface IDataWriter
    {
        Task WriteAsync(string location, string payload);
    }
}
