﻿using Moravia.Homework.FormatConverter.Enum;
using System;

namespace Moravia.Homework.FormatConverter.DataWriter
{
    public static class DataWriterFactory
    {
        private static readonly Lazy<FileSystemWriter> _fsWriter = new Lazy<FileSystemWriter>(() => new FileSystemWriter());
        private static FileSystemWriter GetFileSystemWriter() => _fsWriter.Value;

        public static IDataWriter Build(WriterProvider option)
        {
            return option switch
            {
                WriterProvider.FileSystem => GetFileSystemWriter(),
                WriterProvider.AzureBlobStorage => new AzureBlobWriter("", ""), //Load creadentials from (appseting, ...)
                _ => throw new ArgumentOutOfRangeException(),
            };
        }
    }
}
