﻿using System.IO;
using System.Threading.Tasks;

namespace Moravia.Homework.FormatConverter.DataWriter
{
    public class FileSystemWriter : IDataWriter
    {

        public async Task WriteAsync(string location, string payload)
        {
            using (StreamWriter writer = new StreamWriter(location))
            {
                await writer.WriteAsync(payload);
                writer.Close();
            }
        }
    }
}
