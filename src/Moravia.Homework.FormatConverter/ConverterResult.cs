﻿using Moravia.Homework.FormatConverter.Enum;

namespace Moravia.Homework.FormatConverter
{
    public class ConverterResult
    {
        public ConversionError? Error { get; private set; }
        public bool ConversionSuccessful => Error == null;

        private ConverterResult() { }

        public static ConverterResult ConversionSuccessfull => new ConverterResult();

        public static ConverterResult ConversionUnsuccessfull(ConversionError error)
        {
            return new ConverterResult() { Error = error };
        }

    }
}
