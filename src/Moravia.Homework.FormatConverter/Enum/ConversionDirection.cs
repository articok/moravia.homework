﻿namespace Moravia.Homework.FormatConverter.Enum
{
    public enum ConversionDirection
    {
        Xml2Json,
        Xml2JsonCS,
        Json2Xml
    }
}
