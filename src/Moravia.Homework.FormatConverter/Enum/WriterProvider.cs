﻿namespace Moravia.Homework.FormatConverter.Enum
{
    public enum WriterProvider
    {
        FileSystem,
        AzureBlobStorage
    }
}
