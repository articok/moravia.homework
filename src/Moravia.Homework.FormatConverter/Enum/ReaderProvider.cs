﻿namespace Moravia.Homework.FormatConverter.Enum
{
    public enum ReaderProvider
    {
        FileSystem,
        HttpAdress,
        AzureBlobStorage
    }
}
