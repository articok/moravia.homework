﻿namespace Moravia.Homework.FormatConverter.Enum
{
    public enum ConversionError
    {
        ReadingError,
        WritingError,
        InputConversionError,
        OutputConversionError
    }
}
