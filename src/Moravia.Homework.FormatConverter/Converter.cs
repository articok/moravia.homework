﻿using Moravia.Homework.FormatConverter.DataReader;
using Moravia.Homework.FormatConverter.DataWriter;
using Moravia.Homework.FormatConverter.Enum;
using Moravia.Homework.FormatConverter.Exception;
using Moravia.Homework.FormatConverter.FormatConverter;
using Moravia.Homework.FormatConverter.Helper;
using System.Threading.Tasks;

namespace Moravia.Homework.FormatConverter
{
    public class Converter<T> where T : class 
    {
        private string _input;
        private string _output;
        private IDataReader _reader;
        private IDataWriter _writer;
        private IFormatConverter<T> _inputConverter;
        private IFormatConverter<T> _outputConverter;

        private string InputData { get; set; }
        private T Document { get; set; }
        private string OutputData { get; set; }

        private Converter()
        {
        }

        public static Converter<T> NewConverter => new Converter<T>();

        public Converter<T> SetInput(string input, ReaderProvider provider = ReaderProvider.FileSystem)
        {
            _input = input;
            _reader = DataReaderFactory.Build(provider);
            return this;
        }

        public Converter<T> SetOutput(string output, WriterProvider provider = WriterProvider.FileSystem)
        {
            _output = output;
            _writer = DataWriterFactory.Build(provider);
            return this;
        }

        public Converter<T> SetDirection(ConversionDirection direction)
        {
            (_inputConverter, _outputConverter) = FormatConverterFactory.Build<T>(direction);
            return this;
        }

        public async Task<ConverterResult> Convert()
        {
            try
            {
                InputData = await ExecuteHelper.TryAsyncCatchLogThrow(ReadInput, ConversionError.ReadingError);
                Document = ExecuteHelper.TryCatchLogThrow(ConvertInput, ConversionError.InputConversionError);
                OutputData = ExecuteHelper.TryCatchLogThrow(ConvertOutput, ConversionError.OutputConversionError);
                await ExecuteHelper.TryAsyncCatchLogThrow(WriteOutput, ConversionError.WritingError);
            }
            catch (ConversionException ex)
            {
                return ConverterResult.ConversionUnsuccessfull(ex.Error);
            }

            return ConverterResult.ConversionSuccessfull;
        }

        private Task<string> ReadInput() => _reader.ReadAsync(_input);
        private T ConvertInput() => _inputConverter.ToDocument(InputData);
        private string ConvertOutput() => OutputData = _outputConverter.ToString(Document);
        private Task WriteOutput() => _writer.WriteAsync(_output, OutputData);

    }
}
