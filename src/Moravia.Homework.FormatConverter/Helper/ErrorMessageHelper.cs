﻿using Moravia.Homework.FormatConverter.Enum;
using System;

namespace Moravia.Homework.FormatConverter.Helper
{
    public static class ErrorMessageHelper
    {
        public static string GetMessage(ConversionError error)
        {
            return error switch
            {
                ConversionError.ReadingError => "Reading input file failed",
                ConversionError.WritingError => "Writing output file failed",
                ConversionError.InputConversionError => "Input format conversion failed",
                ConversionError.OutputConversionError => "Output format conversion failed",
                _ => throw new NotImplementedException(),
            };
        }
    }
}
