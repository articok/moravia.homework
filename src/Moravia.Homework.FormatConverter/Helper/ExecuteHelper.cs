﻿using Moravia.Homework.FormatConverter.Enum;
using Moravia.Homework.FormatConverter.Exception;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Moravia.Homework.FormatConverter.Helper
{
    public static class ExecuteHelper
    {
        public async static Task<TResult> TryAsyncCatchLogThrow<TResult>(Func<Task<TResult>> f, ConversionError error)
        {
            try
            {
                return await f();
            }
            catch (System.Exception ex)
            {
                Log.Error(ErrorMessageHelper.GetMessage(error), ex);
                throw new ConversionException(error);
            }
        }

        public async static Task TryAsyncCatchLogThrow(Func<Task> f, ConversionError error)
        {
            try
            {
                await f();
            }
            catch (System.Exception ex)
            {
                Log.Error(ErrorMessageHelper.GetMessage(error), ex);
                throw new ConversionException(error);
            }
        }

        public static TResult TryCatchLogThrow<TResult>(Func<TResult> f, ConversionError error)
        {
            try
            {
                return f();
            }
            catch (System.Exception ex)
            {
                Log.Error(ErrorMessageHelper.GetMessage(error), ex);
                throw new ConversionException(error);
            }
        }

    } 
}
