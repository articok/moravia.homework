﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using System;
using System.Threading.Tasks;

namespace Moravia.Homework.FormatConverter.DataReader
{
    public class AzureBlobReader : IDataReader
    {
        private CloudBlobClient _blobClient;

        public AzureBlobReader(string cloudCredentials)
        {
            _blobClient = CloudStorageAccount.Parse(cloudCredentials).CreateCloudBlobClient();
        }

        public Task<string> ReadAsync(string location)
        {
            var blob = new CloudBlockBlob(new Uri(location), _blobClient);
            return blob.DownloadTextAsync();
        }
    }
}
