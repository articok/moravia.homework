﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Moravia.Homework.FormatConverter.DataReader
{
    public class HttpReader : IDataReader
    {
        public async Task<string> ReadAsync(string location)
        {
            using (var client = new HttpClient())
            {
                return await client.GetStringAsync(location);
            }
        }
    }
}
