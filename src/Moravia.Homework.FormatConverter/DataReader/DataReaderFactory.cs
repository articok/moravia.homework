﻿using Moravia.Homework.FormatConverter.Enum;
using System;

namespace Moravia.Homework.FormatConverter.DataReader
{
    public static class DataReaderFactory
    {
        private static readonly Lazy<FileSystemReader> _fsReader = new Lazy<FileSystemReader>(() => new FileSystemReader());
        private static readonly Lazy<HttpReader> _httpReader = new Lazy<HttpReader>(() => new HttpReader());

        private static FileSystemReader GetFileSystemReader() => _fsReader.Value;
        private static HttpReader GetHttpReader() => _httpReader.Value;

        public static IDataReader Build(ReaderProvider option)
        {
            return option switch
            {
                ReaderProvider.FileSystem => GetFileSystemReader(),
                ReaderProvider.HttpAdress => GetHttpReader(),
                ReaderProvider.AzureBlobStorage => new AzureBlobReader(String.Empty), //Load creadentials from (appseting, ...)
                _ => throw new ArgumentOutOfRangeException(),
            };
        }
    }
}
