﻿using System.Threading.Tasks;

namespace Moravia.Homework.FormatConverter.DataReader
{
    public interface IDataReader
    {
        Task<string> ReadAsync(string location);
    }
}
