﻿using System.IO;
using System.Threading.Tasks;

namespace Moravia.Homework.FormatConverter.DataReader
{
    public class FileSystemReader : IDataReader
    {
        public async Task<string> ReadAsync(string location)
        {
            using StreamReader reader = new StreamReader(location);
            return await reader.ReadToEndAsync();
        }
    }
}
