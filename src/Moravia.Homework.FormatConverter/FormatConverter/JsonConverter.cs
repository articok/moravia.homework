﻿using System.Text.Json;

namespace Moravia.Homework.FormatConverter.FormatConverter
{
    public class JsonConverter<T> : IFormatConverter<T> where T : class
    {
        private JsonSerializerOptions _options;

        public JsonConverter(bool useCamelCase = false)
        {
            _options = new JsonSerializerOptions()
            {
                PropertyNamingPolicy = useCamelCase ? JsonNamingPolicy.CamelCase : null
            };
        }

        public T ToDocument(string content)
        {
            return JsonSerializer.Deserialize<T>(content, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            });
        }

        public string ToString(T document)
        {
            return JsonSerializer.Serialize(document, _options);
        }
    }
}
