﻿using Moravia.Homework.FormatConverter.Enum;
using System;

namespace Moravia.Homework.FormatConverter.FormatConverter
{
    public class FormatConverterFactory
    {
        public static (IFormatConverter<T> inputConverter, IFormatConverter<T> outputConverter) Build<T>(ConversionDirection direction) where T : class
        {
            return direction switch
            {
                ConversionDirection.Xml2Json => (new XmlConverter<T>(), new JsonConverter<T>()),
                ConversionDirection.Xml2JsonCS => (new XmlConverter<T>(), new JsonConverter<T>(useCamelCase: true)),
                ConversionDirection.Json2Xml => (new JsonConverter<T>(), new XmlConverter<T>()),
                _ => throw new NotImplementedException(),
            };
        }
    }
}
