﻿namespace Moravia.Homework.FormatConverter.FormatConverter
{
    public interface IFormatConverter<T> where T : class
    {
        T ToDocument(string content);
        string ToString(T document);
    }
}
