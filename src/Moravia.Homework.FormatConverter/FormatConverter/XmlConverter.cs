﻿using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Moravia.Homework.FormatConverter.FormatConverter
{
    public class XmlConverter<T> : IFormatConverter<T> where T : class
    {
        private readonly XmlSerializer _serializer;

        public XmlConverter()
        {
            _serializer = new XmlSerializer(typeof(T));
        }

        public T ToDocument(string content)
        {
            using MemoryStream stream = new MemoryStream(Encoding.Unicode.GetBytes(content ?? ""));
            return (T)_serializer.Deserialize(stream);
        }

        public string ToString(T document)
        {
            using StringWriter textWriter = new StringWriter();
            _serializer.Serialize(textWriter, document);
            return textWriter.ToString();
        }
    }
}
