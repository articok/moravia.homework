﻿using Moravia.Homework.FormatConverter;
using Moravia.Homework.FormatConverter.Enum;
using Moravia.Homework.Test.TestModel;
using System;
using System.Threading.Tasks;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;
using WireMock.Settings;
using Xunit;

namespace Moravia.Homework.Test.ConverterTests
{
    public class ConverterTest
    {
        [Fact]
        public async Task ConverterExceptionMissingInputTest()
        {
            var result = await Converter<Document>
                    .NewConverter
                    .SetInput("", ReaderProvider.FileSystem)
                    .SetDirection(ConversionDirection.Json2Xml)
                    .SetOutput("")
                    .Convert();

            Assert.Equal(ConversionError.ReadingError, result.Error);
        }

        [Fact]
        public async Task ConverterExceptionInputConversionTest()
        {
            var port = new Random().Next(5000, 6000);
            var baseUrl = "http://localhost:" + port;

            var stub = FluentMockServer.Start(new FluentMockServerSettings
            {
                Urls = new[] { "http://+:" + port }
            });

            stub.Given(
                Request
                .Create()
                    .WithPath("/json"))
                .RespondWith(
                    Response.Create()
                        .WithStatusCode(200)
                        .WithHeader("Content-Type", "aplication/json")
                        .WithBody(@"{""Title"":""TestJson"",""Text""""ContentJson""}"));

            var result = await Converter<Document>
                    .NewConverter
                    .SetInput(baseUrl + "/json", ReaderProvider.HttpAdress)
                    .SetDirection(ConversionDirection.Json2Xml)
                    .SetOutput("")
                    .Convert();

            Assert.Equal(ConversionError.InputConversionError, result.Error);
        }

        [Fact]
        public async Task ConverterExceptionMissingOutputTest()
        {
            var port = new Random().Next(5000, 6000);
            var baseUrl = "http://localhost:" + port;

            var stub = FluentMockServer.Start(new FluentMockServerSettings
            {
                Urls = new[] { "http://+:" + port }
            });

            stub.Given(
                Request
                .Create()
                    .WithPath("/json"))
                .RespondWith(
                    Response.Create()
                        .WithStatusCode(200)
                        .WithHeader("Content-Type", "aplication/json")
                        .WithBody(@"{""Title"":""TestJson"",""Text"":""ContentJson""}"));

            var result = await Converter<Document>
                    .NewConverter
                    .SetInput(baseUrl + "/json", ReaderProvider.HttpAdress)
                    .SetDirection(ConversionDirection.Json2Xml)
                    .SetOutput("")
                    .Convert();

            Assert.Equal(ConversionError.WritingError, result.Error);
        }
    }
}
