﻿using Moravia.Homework.FormatConverter.FormatConverter;
using Moravia.Homework.Test.TestModel;
using Xunit;

namespace Moravia.Homework.Test.FormatConverterTests
{
    public class JsonConverterTest
    {
        private const string json = @"{""Title"":""TestJson"",""Text"":""ContentJson""}";

        [Fact]
        public void Correct2JsonConversion()
        {
            var converter = new JsonConverter<Document>();
            var document = new Document() { Title = "TestJson", Text = "ContentJson" };

            var formated_json = converter.ToString(document);

            Assert.Equal(json, formated_json);
        }

        [Fact]
        public void Correct2DocumentConversion()
        {
            var converter = new JsonConverter<Document>();
            var document = converter.ToDocument(json);

            Assert.Equal("TestJson", document.Title);
            Assert.Equal("ContentJson", document.Text);
        }

        [Fact]
        public void DocJsonDocConversion()
        {
            var input = new Document() { Title = "Json", Text = "JsonProperties" };
            var converter = new JsonConverter<Document>();

            var json = converter.ToString(input);
            var document = converter.ToDocument(json);

            Assert.Equal(input.Title, document.Title);
            Assert.Equal(input.Text, document.Text);
        }
    }
}
