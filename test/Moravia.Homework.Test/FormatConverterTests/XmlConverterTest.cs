﻿using Moravia.Homework.FormatConverter.FormatConverter;
using Moravia.Homework.Test.TestModel;
using System.Xml.Linq;
using Xunit;

namespace Moravia.Homework.Test.FormatConverterTests
{
    public class XmlConverterTest
    {
        [Fact]
        public void Correct2XmlConversion()
        {
            var converter = new XmlConverter<Document>();
            var document = new Document() { Title = "XML Test", Text = "Test" };

            var xml = converter.ToString(document);
            var xdoc = XDocument.Parse(xml);

            Assert.Equal(document.Title, xdoc.Root.Element("Title").Value);
            Assert.Equal(document.Text, xdoc.Root.Element("Text").Value);
        }

        [Fact]
        public void Correct2DocumentConversion()
        {
            var converter = new XmlConverter<Document>();
            var xml = @"<Document>
                          <Title>Test</Title>
                          <Text>Content</Text>
                        </Document>";

            var document = converter.ToDocument(xml);

            Assert.Equal("Test", document.Title);
            Assert.Equal("Content", document.Text);
        }

        [Fact]
        public void DocXmlDocConversion()
        {
            var input = new Document() { Title = "Xml", Text = "XmlFields" };
            var converter = new XmlConverter<Document>();

            var xml = converter.ToString(input);
            var document = converter.ToDocument(xml);

            Assert.Equal(input.Title, document.Title);
            Assert.Equal(input.Text, document.Text);
        }
    }
}
