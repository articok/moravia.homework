﻿using System.Xml.Serialization;

namespace Moravia.Homework.Test.TestModel
{
    public class Document
    {
        [XmlElement("Title")]
        public string Title { get; set; }
        [XmlElement("Text")]
        public string Text { get; set; }
    }
}
