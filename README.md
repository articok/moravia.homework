# README #

Simple Console App for format conversion.

### Example usage ###

* Moravia.Homework.exe --help
* Moravia.Homework.exe -i InputFileLocation -o OutputFileLocation -c Json2Xml

### Please find potential code issues an be able to explain the reason behind it. ###

* hardcoded paths
* IO operations without async approach
* working with stream without proper cleanup
* lack of solid principles
* catching exception without any relevant action